MergeTB Facility
=========

Set up a MergeTB facility `ops` node to bootstrap and maintain the facility.

Requirements
------------

The ops/bastion host must be up and networked, with an internal and external network interface/address.

So far, this role only works on Linux. 

Role Variables
--------------

A description of the settable variables for this role should go here, including any variables that are in defaults/main.yml, vars/main.yml, and any variables that can/should be set via parameters to the role. Any variables that are read from other roles and/or the global scope (ie. hostvars, group vars, etc.) should be mentioned here as well.

Dependencies
------------

N/A

Example Playbook
----------------

run check play: ansible-playbook -i ../hosts.yml --extra-vars @../vars.yml ../playbook.yml -KDC
where hosts.yml is the ansible inventory, vars.yml is the required variables, and playbook.yml is as below.

    - hosts: servers
      roles:
         - { role: mergetb.mergetb-facility, opsuser: mdops }

License
-------

ASL2.0

Author Information
------------------

An optional section for the role authors to include contact information, or a website (HTML is not allowed).
