---
# set up for ntp/ntpsec
# configuration is extremely basic.
- name: (ntpd.yml) ensure ntpd ({{ ntpd_package }}) is installed
  package:
    name: "{{ ntpd_package }}"
    state: installed

# isc ntpd compatible configurations
# this could probably be converted to a template
# just has to consider ntpsec vs ntpd
- name: (ntpd.yml) ntpd compatible configuration
  block:
  - name: (ntpd.yml) check for ipv4 default
    ansible.builtin.command:
      cmd: grep 'restrict -4' {{ ntpd_conf }}
    register: ntpd_ipv4_defaults
    ignore_errors: true
    check_mode: no
  - name: (ntpd.yml) ntpd ensure default ignore v4
    ansible.builtin.lineinfile:
      path: "{{ ntpd_conf }}"
      regexp: '^restrict -4 default'
      line: restrict -4 default ignore
    when: ntpd_ipv4_defaults is succeeded

  - name: (ntpd.yml) check for ipv6 default
    ansible.builtin.command:
      cmd: grep 'restrict -6' {{ ntpd_conf }}
    register: ntpd_ipv6_defaults
    check_mode: no
    ignore_errors: true
  - name: (ntpd.yml) ntpd ensure default ignore v6
    ansible.builtin.lineinfile:
      path: "{{ ntpd_conf }}"
      regexp: '^restrict -6 default'
      line: restrict -6 default ignore
    when: ntpd_ipv6_defaults is succeeded

  - name: (ntpd.yml) check for ntpd no ipversion default
    ansible.builtin.command:
      cmd: grep 'restrict default' {{ ntpd_conf }}
    register: ntpd_ip_defaults
    ignore_errors: true
    check_mode: no
  - name: (ntpd.yml) ntpd ensure default ignore no ip versioning
    ansible.builtin.lineinfile:
      path: "{{ ntpd_conf }}"
      regexp: '^restrict default'
      line: restrict default ignore
    when: ntpd_ip_defaults is succeeded

  # TODO: we should probably change this entire block to a template to specify these directly
  - name: (ntpd.yml) ntpd allow for 10 networks
    ansible.builtin.lineinfile:
      path: "{{ ntpd_conf }}"
      regexp: 'restrict 10.0.0.0'
      line: restrict 10.0.0.0 mask 255.0.0.0 nomodify notrap nopeer
  - name: (ntpd.yml) ntpd allow for 172 networks
    ansible.builtin.lineinfile:
      path: "{{ ntpd_conf }}"
      regexp: 'restrict 172.16.0.0'
      line: restrict 172.16.0.0 mask 255.240.0.0 nomodify notrap nopeer
  - name: (ntpd.yml) ntpd allow localhost networks
    ansible.builtin.lineinfile:
      path: "{{ ntpd_conf }}"
      regexp: 'restrict 127.0.0.0'
      line: restrict 127.0.0.0 mask 255.0.0.0 nomodify notrap nopeer
  - name: (ntpd.yml) ntpd allow 192 networks
    ansible.builtin.lineinfile:
      path: "{{ ntpd_conf }}"
      regexp: 'restrict 192.168.0.0'
      line: restrict 192.168.0.0 mask 255.255.0.0 nomodify notrap nopeer

  when: ntpd_package != "chrony"
  notify: restart {{ ntpd_servicename }}

- name: (ntpd.yml) chronyd specific ntp configuration
  block:
  - name: (ntpd.yml) ensure internal_network ({{ internal_network }}) allowed to query
    ansible.builtin.lineinfile:
      path: "{{ ntpd_conf }}"
      line: allow {{ internal_network }}
      insertafter: '^#allow 192.168.0.0/16'
      validate: chronyd -d -d -U -p -f %s
  - name: (ntpd.yml) ensure unroutable 172 allowed to query
    ansible.builtin.lineinfile:
      path: "{{ ntpd_conf }}"
      line: allow 172.16.0.0/12
      insertafter: '^allow {{ internal_network }}'
      validate: chronyd -d -d -U -p -f %s
  - name: (ntpd.yml) ensure unroutable 192.168 allowed to query
    ansible.builtin.lineinfile:
      path: "{{ ntpd_conf }}"
      line: allow 192.168.0.0/16
      insertafter: '^allow 172.16.0.0/12'
      validate: chronyd -d -d -U -p -f %s
  # optionally, perhaps set the upstream servers or pool (TODO)

  when: ntpd_package == "chrony"
  notify: restart {{ ntpd_servicename }}

- name: (ntpd.yml) ensure ntpd service started
  service:
    name: "{{ ntpd_servicename }}"
    enabled: yes
    state: started
